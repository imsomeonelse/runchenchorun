﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TouchScript : MonoBehaviour {
	GameObject gObj = null;
	Plane objPlane;
	Vector3 m0;

	Vector3 startPos, endPos, direction;
	float touchTimeStart, touchTimeFinish, timeInterval;
	Rigidbody rb;
	Collider col;
	string objectName;

	bool throwAllowed = true;

	bool scriptAdded = false;

	[Range (1f, 50f)] 			  // slider for inspector window
	public float throwForce = 1f; // to control throw force

	private float minX, maxX, minY, maxY, playerRadius;

	Ray GenerateTouchRay(Vector3 touchPos) {
		Vector3 touchPosFar = new Vector3 (touchPos.x, touchPos.y, Camera.main.farClipPlane);
		Vector3 touchPosNear = new Vector3 (touchPos.x, touchPos.y, Camera.main.nearClipPlane);
		Vector3 touchPosF = Camera.main.ScreenToWorldPoint (touchPosFar);
		Vector3 touchPosN = Camera.main.ScreenToWorldPoint (touchPosNear);
		Ray mr = new Ray (touchPosN, touchPosF - touchPosN);
		return mr;
	}

	void Start() {
		float camDistance = Vector3.Distance(transform.position, Camera.main.transform.position);
		Vector2 bottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0,0, camDistance));
		Vector2 topCorner = Camera.main.ViewportToWorldPoint(new Vector3(1,1, camDistance));

		playerRadius = 0.5f;//Change to programatically calculate

		minX = bottomCorner.x + playerRadius;
		maxX = topCorner.x - playerRadius;
		minY = bottomCorner.y + playerRadius + 1.0f;//Include floor size
		maxY = topCorner.y - playerRadius;

		rb = GetComponent<Rigidbody> (); // Get RigidBody2D component
		col = GetComponent<Collider> ();
		objectName = gameObject.name; //Saving the Name of each of the Game Objects´
		//Assigning a unique name for each object
		//
		gameObject.name = "Obj"+Mathf.Abs(gameObject.GetInstanceID()).ToString();
	}

	void FixedUpdate() {
		//If i touch with the first finger
		if(Input.touchCount > 0){
			//If i start my dragg
			if (Input.GetTouch (0).phase == TouchPhase.Began) {
				//Declaring the Ray
				Ray touchRay = GenerateTouchRay (Input.GetTouch (0).position);
				RaycastHit hit;

				//If my Dragg is from a Draggable Object
				if (Physics.Raycast (touchRay.origin, touchRay.direction, out hit)) {
					gObj = hit.transform.gameObject;

					//Checks if I'm currently touching the right object to move only it and not the others
					if (gObj.name == objectName) {

						//Removes game object from tile
						gObj.transform.parent = null;
						//Resizes box collider
						GetComponent<BoxCollider>().size = new Vector3(1f, 1f, 1f);
						//Removes gravity so that the object doesn't keep falling
						rb.useGravity = false;

						//i think this line is not used
						objPlane = new Plane (Camera.main.transform.forward *= 1, gObj.transform.position);

						//Calculates touch offset
						Ray mRay = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
						float rayDistance;
						objPlane.Raycast (mRay, out rayDistance);
						m0 = gObj.transform.position - mRay.GetPoint (rayDistance);

						//For swiping
						touchTimeStart = Time.time;
						startPos = m0;
					}
				}
			}
			
            //If i hold the Object in my finger
			else if (Input.GetTouch (0).phase == TouchPhase.Moved && gObj && gObj.name == objectName) {
				Ray mRay = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				float rayDistance;
				if (objPlane.Raycast (mRay, out rayDistance)) {
					gObj.transform.position = mRay.GetPoint (rayDistance) + m0;

					//BOUNDARIES
					Vector3 pos = gObj.transform.position;

					// horizontal constraint
					if(pos.x < minX) pos.x = minX;
					if(pos.x > maxX) pos.x = maxX;

					// vertical constraint
					if(pos.y < minY) pos.y = minY;
					if(pos.y > maxY) pos.y = maxY;

					gObj.transform.position = pos;

					startPos = gObj.transform.position;
				}
			}

			//If i drop the Object to throw it
			else if (Input.GetTouch (0).phase == TouchPhase.Ended && gObj && gObj.name == objectName && throwAllowed) {
				rb.useGravity = true;
				col.isTrigger = false;

				touchTimeFinish = Time.time;
				// calculate swipe time interval
				timeInterval = touchTimeFinish - touchTimeStart;

				// getting release finger position
				Ray mRay = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				float rayDistance;
				objPlane.Raycast (mRay, out rayDistance);
				endPos = gObj.transform.position - mRay.GetPoint (rayDistance);

				// calculating swipe direction
				direction = startPos - endPos;

				// add force to ball rigidbody depending on swipe time and direction
				rb.AddForce (new Vector3 (direction.x / timeInterval, -direction.y / timeInterval, 0) * throwForce, ForceMode.Impulse);
				// one attempt to throw a ball only
				gObj = null;
			}
		}
	}

	void OnCollisionEnter(Collision col) {
		if(col.gameObject.name == "Floor" && !scriptAdded) {
			//Object.Destroy(gameObject, 3.0f);
			gameObject.AddComponent<BackgroundSpeed>();
			scriptAdded = true;
		}
		if (col.gameObject.tag == "Item") {
			Debug.Log ("IgnoreCollisionn");
			Physics.IgnoreCollision(col.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
		}
	}
}﻿
