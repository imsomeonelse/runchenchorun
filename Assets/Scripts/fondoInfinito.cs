﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class fondoInfinito : MonoBehaviour {

	//Shown Tiles Container
	public GameObject tilesPool;
	//All Tiles Container
	public GameObject[] tilesPool_container;
	//Previous Tile
	public GameObject previousTile;
	//Next Tile
	public GameObject nextTile;
	//Camera Game Object
	public GameObject mainCamera_container;
	//Camera Properties
	public Camera cameraComponent;
	//Boolean for start movement
	public bool play;
	//Boolean for detect Tile
	public bool tileOutOfScreen;
	//Total Tiles Count
	int tilesCount = 0;
	//Random Selected Tile
	int tileSelected ;
	//Tile Size
	public float tileSize = 0;
	//Plane Size
	public float planeSize = 0;
	//Movement Speed
	public float speed = 16;
	//Speed Increase
	public float speedIncrease = 2;
	//Subtile Size
	public float tamañoPieza = 0;
	//Screen Size
	public Vector3 screenSize;

	// Use this for initialization
	void Start () {
		tilesPool = GameObject.Find ("tilesPool");
		mainCamera_container = GameObject.Find ("MainCamera");
		cameraComponent = mainCamera_container.GetComponent<Camera> ();
		setMovementSpeed ();
		//getScreenSize ();

		moveTiles ();
	}

	//Method thats set movement'speed
	void setMovementSpeed() {
		InvokeRepeating("TimeoutHandler", 1.0f, 10f);
	}
	
    //Method for incrementing the speed
	void TimeoutHandler() {
		speed += speedIncrease;
	}

	//Method for getting the Tiles and storing them in an array
	void moveTiles() {
		tilesPool_container = GameObject.FindGameObjectsWithTag ("tile");
		for (int i = 0; i < tilesPool_container.Length; i++) {
			//this line removes the game objects and paste them into the Pool
			tilesPool_container[i].gameObject.transform.parent = tilesPool.transform;
			tilesPool_container[i].gameObject.name = "tile_desactivated"+i;
			tilesPool_container[i].gameObject.SetActive (false);
		}
		instantiateTiles();
	}

	//Method for Showing Tiles by copy them into the shown main folder
	void instantiateTiles() {
		tilesCount++;
		tileSelected =	Random.Range(0, tilesPool_container.Length);
		GameObject newTile = Instantiate(tilesPool_container[tileSelected]);
		newTile.SetActive (true);
		newTile.name = "Tile"+tilesCount;
		//Assigns the parent of the new tile in this case TilesShown
		newTile.transform.parent = gameObject.transform;
		setTilesPosition ();

	}

	//Method for Position the Tiles
	void setTilesPosition() {
		previousTile = GameObject.Find ("Tile"+(tilesCount-1));
		nextTile = GameObject.Find("Tile"+tilesCount);
		getTileSize ();
		nextTile.transform.position = new Vector3( (previousTile.transform.position.x + tileSize),previousTile.transform.position.y,0);
		tileOutOfScreen = false;
	}

	//Method for get Tile Size
	void getTileSize() {
		for(int i = 0; i < previousTile.transform.childCount; i++) {
			if( previousTile.transform.GetChild(i).gameObject.tag == "plane" ) {
				 planeSize = previousTile.transform.GetChild(i).gameObject.GetComponent<Renderer>().bounds.size.x;
			}
		}
		tileSize = tileSize + planeSize;
	}

	// Screen size updater
	void getScreenSize() {
		screenSize = new Vector3 (cameraComponent.ScreenToWorldPoint (new Vector3 (0, 0, 0)).x - 0.5f, 0, 0);
	}

	// Update is called once per frame
	void Update () {
		//if (play) {
			transform.Translate (Vector3.left * speed * Time.deltaTime);
			if (previousTile.transform.position.x + tileSize < screenSize.x && tileOutOfScreen == false) {
				tileOutOfScreen = true;
				cleanTiles ();
			}
		//}
	}

	// Method for destroying the unshown tiles
	void cleanTiles () {
		Destroy(previousTile);
		tileSize = 0;
		previousTile = null;
		instantiateTiles ();
	}
}
