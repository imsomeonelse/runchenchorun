﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSpeed : MonoBehaviour {
	//Movement Speed
	public float speed = 5f;
	//Speed Increase
	public float speedIncrease = 0.25f;

	// Use this for initialization
	void Start () {
		setMovementSpeed ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.left * speed * Time.deltaTime);
	}

	//Method thats set movement'speed
	void setMovementSpeed()
	{
		InvokeRepeating("TimeoutHandler", 1.0f, 10f);
	}
	//Method for incrementing the speed
	void TimeoutHandler() {
		speed += speedIncrease;
	}
}
