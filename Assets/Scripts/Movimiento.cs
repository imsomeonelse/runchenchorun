﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movimiento : MonoBehaviour {

    public float speed = 0.1f;
    public float distancePerSecond = 0.1f;
    Vector3 initialPosition;
    float secondsCounter = 0;
    float secondsToCount = 1;
    int contadorChoques = 0;
    bool jumping = false;
    bool animacion = false;
    public mechanimController mechanicScript;

    // Use this for initialization
    void Start () {
        mechanicScript = GetComponent<mechanimController>();
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate () {
        characterMove();
        isJumping();
    }

    void characterMove(){
      secondsCounter += Time.deltaTime;
      if (secondsCounter >= secondsToCount)
      {
          float posX = transform.position.x + (1 * speed * Time.deltaTime);
          transform.position = new Vector3(Mathf.Clamp(posX, -12.5f, 12.5f), transform.position.y, transform.position.z);
      }
      float posY = transform.position.y + (2.0f * speed * Time.deltaTime);
      float posYBack = transform.position.y - (2.0f * speed * Time.deltaTime);
    }

    void isJumping()
    {

       if( GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Jumping") )  
       {
          GetComponent<BoxCollider>().size = new Vector3(1f, 1f, 1f);
          jumping = true;
       }
       else
       {
          GetComponent<BoxCollider>().size = new Vector3(5f, 5f, 5f);
          jumping = false;
       }

       if( GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("dodge") )  
       {
          GetComponent<BoxCollider>().size = new Vector3(1f, 1f, 1f);
          jumping = false;
       }
        
      
    }

    //Dog Collider Controllers
    void OnTriggerEnter(Collider other)
    {
        //GAME OVER
    		if (other.gameObject.name == "GameOver")
    		{
    			SceneManager.LoadScene ("Menu");
    		}

        //ChOCADO
        if (other.gameObject.name == "Obstaculo")
        {
          mechanicScript.Knock();
        }

        // SALTO
        if (other.gameObject.name == "SaltoEntrada")
        {
          //Jump
          mechanicScript.Jump();
        }

        // GOLPEADO
        if (other.gameObject.tag == "Item")
        {
          if (jumping)
          {
            Debug.Log("Get Hited");
            mechanicScript.getHited();
          }
          else{
            Debug.Log("Get Dodged");
            mechanicScript.Dodge();
          }
                      
        }

    }
}
