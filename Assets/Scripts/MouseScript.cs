﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseScript : MonoBehaviour {
	GameObject gObj = null;
	Plane objPlane;
	Vector3 m0;

	Vector3 startPos, endPos, direction;
	float touchTimeStart, touchTimeFinish, timeInterval;
	Rigidbody rb;
	Collider col;
	string objectName;

	bool throwAllowed = true;
	public float throwForce = 1f;

	Vector2 firstPressPos;
	Vector2 secondPressPos;
	Vector2 currentSwipe;

	private float minX, maxX, minY, maxY, playerRadius;

	Ray GenerateTouchRay(Vector3 touchPos){
		Vector3 touchPosFar = new Vector3 (touchPos.x, touchPos.y, Camera.main.farClipPlane);
		Vector3 touchPosNear = new Vector3 (touchPos.x, touchPos.y, Camera.main.nearClipPlane);
		Vector3 touchPosF = Camera.main.ScreenToWorldPoint (touchPosFar);
		Vector3 touchPosN = Camera.main.ScreenToWorldPoint (touchPosNear);
		Ray mr = new Ray (touchPosN, touchPosF - touchPosN);
		return mr;
	}

	void Start()
	{
		float camDistance = Vector3.Distance(transform.position, Camera.main.transform.position);
		Vector2 bottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0,0, camDistance));
		Vector2 topCorner = Camera.main.ViewportToWorldPoint(new Vector3(1,1, camDistance));

		playerRadius = 0.5f;//Change to programatically calculate

		minX = bottomCorner.x + playerRadius;
		maxX = topCorner.x - playerRadius;
		minY = bottomCorner.y + playerRadius + 1.0f;//Include floor size
		maxY = topCorner.y - playerRadius;

		rb = GetComponent<Rigidbody> (); // Get RigidBody3d component
		col = GetComponent<Collider> ();
		objectName = gameObject.name; //Saving the Name of each of the Game Objects
	}

	void FixedUpdate() {

		 //When press the mouse
		if (Input.GetMouseButtonDown(0))

		{
			firstPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			//Check if the obbject Pressed is the one with this script
			if (Physics.Raycast(ray.origin, ray.direction, out hit)) {
				gObj = hit.transform.gameObject;

				if (gObj.name == objectName) {
					gObj.transform.parent = null;
					//Resizes box collider
					GetComponent<BoxCollider>().size = new Vector3(1f, 1f, 1f);
					//Removes gravity so that the object doesn't keep falling
					rb.useGravity = false;

					//i think this line is not used, Check with Sue
					objPlane = new Plane (Camera.main.transform.forward *= 1, gObj.transform.position);

					//Calculates touch offset
					Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
					float rayDistance;
					objPlane.Raycast (mRay, out rayDistance);
					m0 = gObj.transform.position - mRay.GetPoint (rayDistance);

					//For swiping
					touchTimeStart = Time.time;
					startPos = m0;
				}
			}
		}
		if (Input.GetMouseButton (0))
		{
			//save ended touch 2d point
			secondPressPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
			//normalize the 2d vector
			currentSwipe.Normalize();
			if (currentSwipe != secondPressPos && gObj && gObj.name == objectName)

			{
				Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
				float rayDistance;
				if (objPlane.Raycast (mRay, out rayDistance)) {
					gObj.transform.position = mRay.GetPoint (rayDistance) + m0;

					//BOUNDARIES
					Vector3 pos = gObj.transform.position;

					// horizontal constraint
					if(pos.x < minX) pos.x = minX;
					if(pos.x > maxX) pos.x = maxX;

					// vertical contraint
					if(pos.y < minY) pos.y = minY;
					if(pos.y > maxY) pos.y = maxY;

					gObj.transform.position = pos;

					startPos = gObj.transform.position;
				}
			 }

		}
		if (Input.GetMouseButtonUp (0))

		{
			//save ended touch 2d point
			secondPressPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

			//normalize the 2d vector
			currentSwipe.Normalize();



		 	if ( currentSwipe != secondPressPos && gObj && gObj.name == objectName && throwAllowed)

		 	{
				rb.useGravity = true;
				//col.isTrigger = false;

				touchTimeFinish = Time.time;
				// calculate swipe time interval
				timeInterval = touchTimeFinish - touchTimeStart;

				// getting release finger position
				Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
				float rayDistance;
				objPlane.Raycast (mRay, out rayDistance);
				endPos = gObj.transform.position - mRay.GetPoint (rayDistance);

				// calculating swipe direction
				direction = startPos - endPos;

				// add force to ball rigidbody depending on swipe time and direction
				//Debug.Log("Force: "+new Vector3 (direction.x / timeInterval, direction.y / timeInterval, 0) * throwForce, ForceMode.Acceleration);
				rb.AddForce (new Vector3 (direction.x / timeInterval, -direction.y / timeInterval, 0) * throwForce, ForceMode.Impulse);
				// one attempt to throw a ball only
				gObj = null;
			}
		}
	}
}
