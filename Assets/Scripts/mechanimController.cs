﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mechanimController : MonoBehaviour {

	public Animator	animController;
	// Use this for initialization
	void Start () {
		animController = GetComponent<Animator>();
	}
	public void Jump(){
		animController.SetTrigger("jump");
	}
	public void Knock(){
		animController.SetTrigger("knock");
	}
	public void getHited(){
		animController.SetTrigger("hited");
	}
	public void Dodge(){
		animController.SetTrigger("dodge");
	}

}
