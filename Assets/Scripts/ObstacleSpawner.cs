﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {
	public GameObject obstacle;

	public float minWait = 1f;
	public float maxWait = 10f;

	private float time;
	private float spawnTime;

	// Use this for initialization
	void Start () {
		SetRandomTime ();
		time = minWait;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		time += Time.deltaTime;

		if (time >= spawnTime) {
			SpawnObstacle ();
			SetRandomTime ();
		}
	}

	void SpawnObstacle() {
		time = 0;
		Instantiate(obstacle, gameObject.transform.position, obstacle.transform.rotation);
	}

	void SetRandomTime(){
		spawnTime = Random.Range (minWait, maxWait);
	}
}
